#
#theme - first contact with unknown intelligence
#

try:
    import sys
    import win32api
    win32api.SetDllDirectory(sys._MEIPASS)
except:
    pass

from pathlib import Path
import os

try:
    WORKING_DIR = Path(sys._MEIPASS)
except AttributeError:
    WORKING_DIR = Path(os.getcwd())

import curses
import random
import string
import time

from curses import textpad
from typing import List, Tuple

import sound
import term

from editor import Editor
from view import View
from mytypes import Buffer, contents, log, stdscr

from term import FG, YELLOW

def init_curses():
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(True)
    curses.curs_set(0)

FS = sound.init_sf()
TRANSLATE_EMOJIS = True

class interpreter:
    def decode(self, code):
        pass

class maze:
    def __init__(self):
        self.scrh = 30
        self.scrw = 80
        self.map = []
        self.chars_to_find = {104: '🌓', 308: '🔴', 294: '🔵', 468: '⋈', 754: '🔶', 1503: '🞜', 2073: '～', 3075: 'ꚩ', 3216: '🔷', 3881: 'ꯠ'}

        map = (WORKING_DIR/'map.txt').open()
        a = 0
        for line in map:
            self.map.append([])
            for char in line:
                if char != '\n':
                    self.map[a].append(char)
            a += 1

        self.map_size = (len(self.map), len(self.map[0])) # 50, 100

        # View
        self.y_movement = 0
        self.x_movement = 0

        self.door_music = {
            1013: '🌓🔴🔴🔴',
            378: '🌓🔴🔵🞜🔴🔵🞜🔶🔶',
            593: '',
            2873: '🌓ꚩ🔷⋈⋈⋈🔶🔵🔴🔵🔶🞜ꚩ🔷',
            3213: '🌓ꚩ🔴⋈🔵🔶ꚩ🔵🞜⋈⋈🔵🔶🔵ꚩ🔴',
            3533: '',
            3034: '',
        }

    def generate_music(self):
        note_chars = {'🔴', '🔵', '🔶', '🔷'}
        chars = ['🔴', '🔵', '⋈', '🔶', '🞜', '～', 'ꚩ', '🔷', 'ꯠ']
        result = ''
        note_chars_count = 0
        while note_chars_count < 5:
            note_chars_count = 0
            result = ''.join(random.choice(chars) for _ in range(random.randint(10,20)))
            for c in result:
                if c in note_chars:
                    note_chars_count += 1
        return result


    def pos_to_dungeon(self, pos):
        return (pos[0] + self.y_movement, pos[1] + self.x_movement)

    def refreshMap(self, dungeon_wnd, human, screen, collected):

        if human.y > self.scrh // 2 and self.scrh + self.y_movement - human.y < self.map_size[0]:
            self.y_movement += 1
            human.y -= 1
            human.relative_y += 1
        elif human.y < self.scrh // 2 and human.relative_y > 0:
            self.y_movement -= 1
            human.y += 1
            human.relative_y -= 1

        if human.x > self.scrw // 2 and self.scrw + self.x_movement - human.x < self.map_size[1]:
            self.x_movement += 1
            human.x -= 1
            human.relative_x += 1
        elif human.x < self.scrw // 2 and human.relative_x > 0:
            self.x_movement -= 1
            human.x += 1
            human.relative_x -= 1

        if collected:
            for row_index in range(self.map_size[0]):
                for column_index in range(self.map_size[1]):
                    try:
                        char = self.map[row_index][column_index]
                        if char == '!':
                            dungeon_wnd.addch(row_index, column_index, self.map[row_index][column_index],
                                              curses.color_pair(1))
                        elif char == '+':
                            dungeon_wnd.addch(row_index, column_index, self.map[row_index][column_index],
                                              curses.color_pair(2))
                        else:
                            dungeon_wnd.addch(row_index, column_index, self.map[row_index][column_index])
                    except:
                        pass

        # screen.addstr(human.y, human.x, human.image, curses.color_pair(3))
        dungeon_wnd.addstr(human.y-1+human.relative_y, human.x+human.relative_x-1, human.image, curses.color_pair(3))
        # dungeon_wnd.addstr(human.y-1+human.relative_y, human.x+human.relative_x, human.image, curses.color_pair(3))
        dungeon_wnd.refresh(self.y_movement, self.x_movement+1, 1, 1, self.scrh, self.scrw)
        dungeon_wnd.addstr(human.y-1+human.relative_y, human.x+human.relative_x-1, ' ', curses.color_pair(3))
        screen.refresh()


    def is_valid(self, pos):
        return (1 <= pos[0] <= self.map_size[0]) and (1 <= pos[1] <= self.map_size[1])

    def open_door(self, door_pos, dungeon_wnd, screen, human):
        self[door_pos] = ' '
        door_y, door_x = door_pos
        dungeon_wnd.addch(door_y, door_x, ' ')
        dungeon_wnd.refresh(self.y_movement, self.x_movement, 1, 1, self.scrh, self.scrw)
        screen.addstr(human.y, human.x, human.image, curses.color_pair(3))

    def __getitem__(self, pos):
        return self.map[pos[0]][pos[1]]

    def __setitem__(self, pos, val):
        self.map[pos[0]][pos[1]] = val

class hero:
    def __init__(self):
        self.y = 2
        self.x = 1
        self.relative_y = 0
        self.relative_x = 0
        self.image = '@'
        self.collected = []
        self.number = 0

    @property
    def mpos_y(self):
        return self.y + self.relative_y - 1

    @property
    def mpos_x(self):
        return self.x + self.relative_x - 1

    def get_maze_item(self, maze):
        return maze.map[self.mpos_y][self.mpos_x]

    @property
    def mpos(self):
        return (self.mpos_y, self.mpos_x)

    DIR_DELTA = {
        curses.KEY_LEFT: (0, -1),
        curses.KEY_RIGHT: (0, 1),
        curses.KEY_UP: (-1, 0),
        curses.KEY_DOWN: (1, 0),
    }

    def neighbour_mpos(self, direction):
        delta_y, delta_x = self.DIR_DELTA[direction]
        return (self.mpos_y+delta_y, self.mpos_x+delta_x)

    def is_valid(self, maze, pos):
        return (1 <= pos[1] < maze.map_size[1]) and (1 <= pos[0] < maze.map_size[0])

    def is_dir(self, key):
        return key in self.DIR_DELTA

    def move_me(self, direction):
        delta_y, delta_x = self.DIR_DELTA[direction]
        self.y += delta_y
        self.x += delta_x

    def door_in_sight(self, maze):
        for direction in [curses.KEY_UP, curses.KEY_DOWN, curses.KEY_LEFT, curses.KEY_RIGHT]:
            nbr_pos = self.neighbour_mpos(direction)
            if maze[nbr_pos] == '+':
                door_index = nbr_pos[0] * maze.map_size[1] + nbr_pos[1]
                return door_index, nbr_pos
        return False, False

    def move(self, direction, maze, screen, dungeon_wnd, inventory):
        change = False
        nbr_pos = self.neighbour_mpos(direction)
        nbr_item = maze[nbr_pos]

        # Wall
        if nbr_item == '*':
            return

        # Door
        if nbr_item == '+':
            door_index = nbr_pos[0] * maze.map_size[1] + nbr_pos[1]
            player = sound.Score(FS)
            if door_index == 3533:
                music = '🌓' + maze.generate_music()
                maze.door_music[door_index] = music
            player.play(maze.door_music[door_index])
            if door_index == 3034:
                end_animation(screen)
            return

        # No wall or door, we can move there
        self.move_me(direction)

        # Collect items
        collected = False
        maze_item = maze[self.mpos]

        if maze_item != ' ':
            # Delete the item from the map
            maze[self.mpos] = ' '
            collected = True

            # Add it to the collection
            collected_char = maze.chars_to_find[self.mpos_y * maze.map_size[1] + self.mpos_x]
            if TRANSLATE_EMOJIS:
                collected_char = sound.Score.translate_to_win(collected_char)
            self.collected.append(collected_char)

            # Refresh the inventory window
            print_centered_text(inventory, f'Press {len(self.collected)-1} for {collected_char}', 1 + len(self.collected), 19)
            self.number += 1
            inventory.refresh()

        maze.refreshMap(dungeon_wnd, self, screen, collected)


MIN_CONSOLE_HEIGHT = 34
MIN_CONSOLE_WIDTH = 102


def main(stdscr):
    stdscr.clear()
    stdscr.refresh()

    pressed_key = 0
    screen_height, screen_width = stdscr.getmaxyx()

    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)

    bgs = sound.BGSound(FS)
    bgs.start()

    if screen_height < MIN_CONSOLE_HEIGHT or screen_width < MIN_CONSOLE_WIDTH:
        print_centered_text(stdscr, '🎜🎝🎜', screen_height // 2 - 2, screen_width)
        print_centered_text(stdscr, f'Please resize your window to at least {MIN_CONSOLE_WIDTH} cols x {MIN_CONSOLE_HEIGHT} lines.', screen_height // 2, screen_width, standout=True)
        print_centered_text(stdscr, '(or press q to quit...)', screen_height // 2 + 2, screen_width)

        CONTINUE = False
        while pressed_key != ord('q'):
            stdscr.refresh()
            if pressed_key == curses.KEY_RESIZE:
                screen_height, screen_width = stdscr.getmaxyx()
                if screen_height >= MIN_CONSOLE_HEIGHT and screen_width >= MIN_CONSOLE_WIDTH:
                    CONTINUE = True
                    break
            pressed_key = stdscr.getch()

        if not CONTINUE:
            bgs.stop()
            return

    stdscr.clear()

    print_centered_text(stdscr, 'Did you know there are entities that can communicate via singing?', screen_height // 3 - 1, screen_width)
    print_centered_text(stdscr, "You won't get far without that in this game...", screen_height // 3, screen_width)
    print_centered_text(stdscr, '🎜🎝🎜', screen_height // 2 - 2, screen_width)
    print_centered_text(stdscr, 'INSIDE THE UNKNOWN', screen_height // 2 - 1, screen_width)
    print_centered_text(stdscr, '🎜🎝🎜', screen_height // 2, screen_width)
    print_centered_text(stdscr, 'Turn on sound in order to play this game', screen_height // 2 + 2, screen_width, standout=True)
    print_centered_text(stdscr, 'Press s to start... (or q to quit)', screen_height // 2 + 3, screen_width)


    while pressed_key != ord('q') and pressed_key != ord('s'):
        stdscr.refresh()
        pressed_key = stdscr.getch()

    bgs.stop()

    if pressed_key == ord('q'):
        return

    elif pressed_key == ord('s'):
        start_animation(stdscr)

        stdscr.clear()
        stdscr.refresh()

        dungeon = maze()
        human = hero()

        # Konsole
        konsole_border = curses.newwin(3, screen_width - 2, screen_height - 3, 1)
        konsole_border.border()
        konsole = konsole_border.derwin(1, screen_width - 4, 1, 1)
        stdscr.refresh()
        konsole_border.refresh()

        # Inventory
        inventory_border = curses.newwin(screen_height - 3, 21, 0, screen_width - 21)
        inventory_border.border()
        print_centered_text(inventory_border, 'INVENTORY', 1, 19, standout=True)
        stdscr.refresh()
        inventory_border.refresh()

        # Map
        dungeon_map = curses.newpad(50, 100)
        for row_index in range(dungeon.map_size[0]):
            for column_index in range(dungeon.map_size[1]):
                try:
                    char = dungeon.map[row_index][column_index]
                    if char == '!':
                        dungeon_map.addch(row_index, column_index, dungeon.map[row_index][column_index],
                                          curses.color_pair(1))
                    elif char == '+':
                        dungeon_map.addch(row_index, column_index, dungeon.map[row_index][column_index],
                                          curses.color_pair(2))
                    else:
                        dungeon_map.addch(row_index, column_index, dungeon.map[row_index][column_index])
                except:
                    pass

        stdscr.addstr(human.y, human.x, human.image, curses.color_pair(3))
        dungeon_map.refresh(0, 0, 1, 1, dungeon.scrh, dungeon.scrw)

        pressed_key = 0

        while pressed_key != ord('q'):

            if pressed_key == ord('c'):
                print_centered_text(stdscr, '''     Sing mode. Use inventory to compose. Press Enter to sing.     ''', 0, screen_width)
                konsole.clear()
                curses.curs_set(2)
                buf: Buffer = [[]]
                view = View(buf, konsole)
                editor = Editor(buf, view)
                while True:
                    pressed_key = stdscr.getch()
                    if editor.is_edit_key(pressed_key):
                        editor.process_edit_key(pressed_key)
                        editor.show_cursor()
                    elif pressed_key == editor.KEY_ENTER:
                        player = sound.Score(FS)
                        src = contents(buf)
                        if TRANSLATE_EMOJIS:
                            src = player.translate_from_win(src)
                        player.play(src)

                        door_index, door_pos = human.door_in_sight(dungeon)
                        if door_pos:
                            if player.equivalent(src, dungeon.door_music[door_index]):
                                dungeon.open_door(door_pos, dungeon_map, stdscr, human)

                        konsole.clear()
                        konsole.refresh()
                        break
                    else:
                        try:
                            item_idx = int(chr(pressed_key))
                            if item_idx < len(human.collected):
                                item = human.collected[item_idx]
                                editor.ins_char(item, refresh=True)
                        except Exception as ex:
                            pass
                    view.refresh_full()
                    editor.show_cursor()
                curses.curs_set(0)

            elif pressed_key in (curses.KEY_UP, curses.KEY_DOWN, curses.KEY_LEFT, curses.KEY_RIGHT):
                human.move(pressed_key, dungeon, stdscr, dungeon_map, inventory_border)

            print_centered_text(stdscr, '''       Movement mode. Press 'c' to compose a song.       ''', 0, screen_width)
            # stdscr.addstr(human.y, human.x, human.image, curses.color_pair(3))
            stdscr.refresh()
            pressed_key = stdscr.getch()
            # stdscr.addstr(human.y, human.x, human.image, curses.color_pair(3))
            stdscr.refresh()
        return

def end_animation(screen):
    screen.clear()
    bg = sound.RandomBackgroundSound(FS)
    bg.randomize()

    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_MAGENTA, curses.COLOR_BLACK)

    possible_directions = [(0, -2), (-1, 0), (0, 2), (1, 0)]
    spiral_length = 1
    direction = 0
    screen_y, screen_x = screen.getmaxyx()
    pos_y, pos_x = screen_y // 2, screen_x // 2

    sleep_duration = 0.01
    while True:
        for iteration in range(spiral_length):
            color = random.randint(0, 6)
            char = random.choice(string.ascii_letters + string.digits)
            try:
                screen.addch(pos_y, pos_x, char, curses.color_pair(color))
            except:
                bg._off()
                # msg = "\033[" + \
                #       ';'.join([chr(x) for x in [38, 2, 255, 255, 0, 0]]) + 'm' + 'You killed the alien :('
                raise Exception(FG(YELLOW)|'You killed the alien :(')
            pos_y += possible_directions[direction][0]
            pos_x += possible_directions[direction][1]
            screen.refresh()
            if iteration % random.randint(5, 20) == 0:
                sleep_duration = random.randint(1, 40)*0.002
            time.sleep(sleep_duration)
            bg.randomize()

        spiral_length += 2
        if direction < 3:
            direction += 1
        else:
            direction = 0

    bg._off()


def start_animation(screen):
    pass

def print_centered_text(screen, message, height, screen_width, standout=False):
    center = len(message) // 2
    position = screen_width // 2 - center
    if standout:
        screen.addstr(height, position, message, curses.A_STANDOUT)
    else:
        screen.addstr(height, position, message)
    stdscr.refresh()

if __name__ == '__main__':
    if sys.platform == 'win32':
        # Reset console size
        # os.system(f"mode con: lines={MIN_CONSOLE_HEIGHT+20} cols={MIN_CONSOLE_WIDTH+80}")

        # Enable VT mode
        import ctypes
        kernel32 = ctypes.windll.kernel32
        kernel32.SetConsoleMode(kernel32.GetStdHandle(-11), 7)
        TRANSLATE_EMOJIS = True
    else:
        TRANSLATE_EMOJIS = False

    init_curses()
    curses.wrapper(main)
