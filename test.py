#from mingus.midi import fluidsynth
from midiutil import MIDIFile
fluidsynth.init("soundfont.SF2")

import fluidsynth

fs = fluidsynth.Synth()
import pygame

import tempfile

pygame.init()

def play_midi(mid: MIDIFile):
    with tempfile.TemporaryFile(suffix='.mid') as tmp:
        mid.writeFile(tmp)
        tmp.flush()
        tmp.seek(0)
        pygame.mixer.music.load(tmp)
        pygame.mixer.music.play()


NOTES_MAP = {
    "C": 0,
    "C#": 1,
    "Db": 1,
    "D": 2,
    "D#": 3,
    "Eb": 3,
    "E": 4,
    "F": 5,
    "F#": 6,
    "Gb": 6,
    "G": 7,
    "G#": 8,
    "Ab": 8,
    "A": 9,
    "A#": 10,
    "Hb": 10,
    "H": 11,
}


def note_to_pitch(note, octave):
    return octave*12 + NOTES_MAP[note]


def play_score(notes, octave):
    notes = notes.split(',')
    midi = MIDIFile(len(notes))
    for idx, note in enumerate(notes):
        midi.addNote(
            track=0,
            channel=1,
            pitch=note_to_pitch(note, octave),
            time=idx,
            duration=1,
            volume=100
        )
    play_midi(midi)

play_score("C,E,G,C,E,G", 3)
