import curses

from mytypes import Pos, Buffer, X, Y, ADD, contents, log, valid_buf_pos
from view import View

class Editor:
    KEY_DEL = 330
    KEY_ENTER = ord('\n')
    MOVE_KEYS = {
        curses.KEY_LEFT: (0, -1),
        curses.KEY_RIGHT: (0, 1),
    }
    EDIT_KEYS = {
        curses.KEY_BACKSPACE,
        KEY_DEL,
    }

    # OP_KEYS = {
    #   7: '', # Ctrl+G
    # }

    def __init__(self, buffer: Buffer, view: View):
        self._buffer = buffer
        self._view = view
        self._cursor_pos: Pos = (0, 0)

    @property
    def cx(self) -> int: return X(self._cursor_pos)

    @property
    def cy(self) -> int: return Y(self._cursor_pos)

    def valid_pos(self, pos: Pos):
        return valid_buf_pos(self._buffer, pos)
        #return (0 <= Y(pos) <= len(self._buffer)) and (0 <= pos[X] <= len(self._buffer[pos[Y]]))

    def ins_char(self, char: str, refresh=True):
        self._buffer[self.cy].insert(self.cx, char)
        self.move_cursor((0, 1), refresh=False)
        if refresh:
            self._view.refresh_line(self.cy)
        self.show_cursor()

    def del_char(self, refresh=True):
        if self.valid_pos(self._cursor_pos):
            del self._buffer[self.cy][self.cx]
            if refresh and self._view.contains(self._cursor_pos):
                self._view.refresh_line(self.cy, self.cx)

    def back_space_char(self, refresh=True):
        self.move_cursor((0, -1), refresh=False)
        self.del_char(refresh=refresh)

    def is_edit_key(self, key: int):
        return key in self.MOVE_KEYS or key in self.EDIT_KEYS

    def process_edit_key(self, key: int, refresh=True):
        if key in self.MOVE_KEYS:
            self.move_cursor(self.MOVE_KEYS[key], refresh=refresh)
        elif key == curses.KEY_BACKSPACE:
            self.back_space_char()
        elif key == self.KEY_DEL:
            self.del_char()
        self.show_cursor()

    def move_view_to_cursor(self, refresh=True):
        delta_x = 0
        delta_y = 0

        if X(self._view.lr_pos) < self.cx:
            delta_x = self.cx - X(self._view.lr_pos)
        elif X(self._view.ul_pos) > self.cx:
            delta_x = self.cx - X(self._view.ul_pos)

        if Y(self._view.lr_pos) < self.cy:
            delta_y = self.cx - X(self._view.lr_pos)
        elif Y(self._view.ul_pos) > self.cy:
            delta_y = self.cy - Y(self._view.ul_pos)

        if delta_x or delta_y:
            self._view.move((delta_x, delta_y), refresh=refresh)
            self.show_cursor()

    def move_cursor(self, delta: Pos, refresh=True):
        delta_y, delta_x = delta
        self._cursor_pos = (self.cy+delta_y, self.cx+delta_x)

        if Y(self._cursor_pos) < 0:
            self._cursor_pos = (0, self.cx)

        while X(self._cursor_pos) < 0:
            self._cursor_pos = (self.cy-1, self.cx)
            if Y(self._cursor_pos) < 0:
                self._cursor_pos = (0, 0)
                break
            self._cursor_pos = (self.cy, self.cx + len(self._buffer[Y(self._cursor_pos)]))

        self.move_view_to_cursor(refresh=refresh)
        self.show_cursor()

    def show_cursor(self):
        self._view.hilight_pos(self._cursor_pos)
