import curses

from typing import List

import term

from mytypes import Pos, Buffer, X, Y, ADD, valid_buf_pos, log



class View:
    def __init__(self, array: Buffer, window):
        self._array = array
        self._window = window
        self._pos = (0, 0)

    @property
    def width(self): return abs(X(self._window.getmaxyx()) - X(self._window.getbegyx()))

    @property
    def ul_pos(self): return self._pos

    @property
    def lr_pos(self): return ADD(self._pos, self._window.getmaxyx())

    def map_to_view(self, pos: Pos):
        return (Y(pos)-Y(self.ul_pos), X(pos)-X(self.ul_pos))

    def content(self):
        ret = []
        for ln in self._array[Y(self.ul_pos):Y(self.lr_pos)]:
            ret.append(ln[X(self.ul_pos):X(self.lr_pos)])
        #print("Content", ret)
        return ret

    def move(self, delta: Pos, refresh=True):
        self._pos = ADD(self._pos, delta)
        if refresh:
            self.refresh_full()

    def move_to(self, pos: Pos, refresh=True):
        self._pos = pos
        if refresh:
            self.refresh_full()

    def contains(self, pos: Pos):
        return Y(self.ul_pos) <= Y(pos) < Y(self.lr_pos) and X(self.ul_pos) <= X(pos) < X(self.lr_pos)

    def refresh_full(self):
        self._window.clear()
        for idx, ln in enumerate(self.content()):
            ln_str = ''.join(ln)
            self._window.addstr(idx, 0, ln_str)
        self._window.refresh()

    def refresh_line(self, ln: int, start_at_x: int = 0):
        self.refresh_full()
        return
        #print("Refresh line", ln, start_at_x)
        if Y(self.lr_pos) <= ln <= Y(self.lr_pos):
            if start_at_x:
                vsx_pos = start_at_x-X(self.ul_pos)
                if vsx_pos >= X(self.lr_pos):
                    return
            content = self._array[ln][start_at_x:X(self.lr_pos)]
            start_pos = self.map_to_view((ln, X(self.ul_pos)))
            #print("Refreshing", content)
            self._window.addstr(Y(start_pos), vsx_pos, content)

    def hilight_pos(self, pos: Pos):
        if self.contains(pos) and valid_buf_pos(self._array, pos):
            vpos = self.map_to_view(pos)
            view_term_pos = self._window.getbegyx()
            term_pos = ADD(view_term_pos, vpos)
            curses.setsyx(Y(term_pos), X(term_pos))
            #self._window.chgat(Y(vpos), X(vpos), 1, curses.A_UNDERLINE)
            self._window.chgat(Y(vpos), X(vpos), 1)
            self._window.refresh()
            #content = term.BG(term.WHITE) | self._array[Y(pos)][X(pos)]
            #self._window.addstr(Y(vpos), X(vpos), content)

