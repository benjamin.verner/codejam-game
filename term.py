"""
    Provides helper functions for terminal output.

    Includes termsize.py (plumbum.cli.termsize)
    taken from Plumbum (https://plumbum.readthedocs.io/en/latest/index.html).
"""

from __future__ import division, print_function, absolute_import

import enum
import os
import platform
import re
import sys

try:
    import termios
    import tty
except:
    pass

from numbers import Number
from struct import Struct

from typing import (
    Any,
    Callable,
    Dict,
    IO,
    Iterable,
    List,
    Match,
    Optional,
    Pattern,
    Tuple,
    Union,
)

try:
    from typing import Literal

    HAVE_LITERAL = True
except:
    HAVE_LITERAL = False
    Literal = List


# See, e.g.,  https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences
_CLEAR_SCREEN_SEQ = "\033[2J\033[1;1H"
_BOLD_CODE = 1
_DIM_CODE = 2
_ITALIC_CODE = 3
_UNDERLINE_CODE = 4
_BLINK_CODE = 25
_REVERSE_CODE = 7
_HIDDEN_CODE = 8
_STRIKEOUT_CODE = 9


class IOMode(enum.IntEnum):
    """
        The components of a semver triple.
    """

    TERM = 0
    PLAIN = 1
    QT = 2


MODE = IOMode.TERM

RGB = Tuple[int, int, int]


class Color:
    """
       Inspired by plumbum.colorlib. A helper class for formatting text using ansi escape sequences.

       Each instance of the class corresponds to a formatting style: a combindation of foreground/background
       color and font style (bold,underline,italic). To apply the style to a string, use the pipe
       operator

       .. code-block:: python

          print(Color(bold=True) | "This text will be in bold")
          print(Color(italic=True) | "And this will be italic")

       which wraps its right argument with the correct ansi sequences (in case the mode is set to TERM,
       if its set to QT, it wraps it in html tags, if set to plain, it returns the string as is)
    """

    _RESET_FG = 39
    _RESET_BG = 49
    _RESET_BOTH = 0
    _RESET_BOLD = 22
    _RESET_UNDERLINE = 24
    _RESET_ITALIC = 23
    _RESET_ALL = 0

    def __init__(
        self,
        fg: Optional[RGB] = None,
        bg: Optional[RGB] = None,
        bold: bool = False,
        underline: bool = False,
        italic: bool = False,
        mode: Optional[IOMode] = None,
    ):
        self.fg = fg
        self.bg = bg
        self.bold = bold
        self.underline = underline
        self.italic = italic
        self._mode = MODE if mode is None else mode

    @classmethod
    def remove_codes(cls, txt: str) -> str:
        """
            Remove any ansi escape sequences from @p txt and
            return the result
        """
        return re.sub(r"\033\[[^m]*m", "", txt)

    @property
    def ansi_sequence(self) -> str:
        """
            The ansi escape sequence for setting the current style
        """
        if self._mode == IOMode.TERM:
            ansi = ";".join(map(str, self.ansi_codes))
            if ansi:
                return "\033[" + ansi + "m"
            return ""
        if self._mode == IOMode.QT:
            style = ""
            if self.bg:
                style += (
                    "background-color: rgb("
                    + ",".join([str(c) for c in self.bg])
                    + ");"
                )
            if self.fg:
                style += "color: rgb(" + ",".join([str(c) for c in self.fg]) + ");"
            ret = """<span style='{style}'>""".format(style=style)
            if self.underline:
                ret += "<u>"
            if self.italic:
                ret += "<i>"
            if self.bold:
                ret += "<b>"
            return ret
        return ""

    @property
    def ansi_codes(self) -> str:
        """The ANSI reset sequence for the full style (color, bold, ...)."""
        codes = []
        if self.fg:
            codes.extend([38, 2, self.fg[0], self.fg[1], self.fg[2]])
        if self.bg:
            codes.extend([48, 2, self.bg[0], self.bg[1], self.bg[2]])
        if self.bold:
            codes.append(_BOLD_CODE)
        if self.underline:
            codes.append(_UNDERLINE_CODE)
        if self.italic:
            codes.append(_ITALIC_CODE)
        return codes

    @property
    def reset(self) -> str:
        """
            Returns the ANSI sequence to reset the terminal back to normal
            as a string, ready to use.
        """
        if self._mode == IOMode.TERM:
            codes = []
            if self.fg:
                codes.append(self._RESET_FG)
            if self.bg:
                codes.append(self._RESET_BG)
            if self.bold:
                codes.append(self._RESET_BOLD)
            if self.underline:
                codes.append(self._RESET_UNDERLINE)
            if self.italic:
                codes.append(self._RESET_ITALIC)
            if codes:
                return "\033[" + ";".join(map(str, codes)) + "m"
            return ""
        if self._mode == IOMode.QT:
            ret = ""
            if self.bold:
                ret += "</b>"
            if self.italic:
                ret += "</i>"
            if self.underline:
                ret += "</u>"
            return ret + "</span>"
        return ""

    def wrap(self, text: str) -> str:
        """
            Wraps text so that the result contains an ANSI sequence to set
            the color followed by text and followed by an ANSI sequence to
            reset the color.
        """
        return self.ansi_sequence + str(text) + self.reset

    def __getitem__(self, key) -> str:
        return self.wrap(key)

    def __or__(self, other) -> str:
        return self.wrap(other)


def FG(col: RGB, mode: Optional[IOMode] = None) -> Color:
    """
        A shortcut to create an instance of the Color class with
        foreground set to `col`.
    """
    return Color(fg=col, mode=mode)


def BG(col: RGB, mode: Optional[IOMode] = None) -> Color:
    """
        A shortcut to create an instance of the Color class with
        background set to `col`.
    """
    return Color(bg=col, mode=mode)


NO_STYLE: Color = Color()

RED: RGB = (255, 0, 0)
YELLOW: RGB = (255, 255, 0)
GREEN: RGB = (0, 170, 0)
BLUE: RGB = (0, 0, 170)
WHITE: RGB = (255, 255, 255)
GRAY: RGB = (128, 128, 128)
BLACK: RGB = (0, 0, 0)

COLOR_OK = FG(GREEN)
COLOR_ERROR = FG(RED)


def format_text(txt: str, mode: Optional[IOMode] = None) -> str:
    """
        Formats a Markdown/HTML-style formatted text for display.

        Arguments:
                txt:  The text to format
                mode: The output medium (e.g., terminal, plain, qt, ...), if
                      it is not specified, term.MODE is used

        The following markup is recognized

        - ``**text**``, ``<b>text</b>``   ... ``text`` is formatted in bold
        - ``*text*``, ``<i>text</i>``     ... ``text`` is formatted in italic
        - ``<u>text</u>``                 ... ``text`` is formatted underlined
        - ``<fg color>text</fg>``, ``<color>text</color>`` ... ``text`` is
                                                formatted in foreground color ``color``
        - ``<bg color>text</bg>``         ... ``text`` is formatted in background color ``color``

        where ``color`` can be any of ``red``, ``yellow``, ``green``, ``white``, ``gray``, ``blue``.
    """
    COLOR_MAP: Dict[str, RGB] = {
        "RED": RED,
        "YELLOW": YELLOW,
        "GREEN": GREEN,
        "WHITE": WHITE,
        "GRAY": GRAY,
        "BLUE": BLUE,
    }
    COL_RE: Pattern = re.compile(
        r"<(?P<coltype>fg|bg)\s+(?P<col>[^>]*)>(?P<text>[^<]*)</(?P=coltype)>",
        re.IGNORECASE,
    )
    COL2_RE: Pattern = re.compile(
        r"<\s*(?P<color>red|yellow|green|blue|white|gray)\s*>(?P<text>[^<]*)</(?P=color)>",
        re.IGNORECASE,
    )

    def col_repl(m: Match) -> str:
        col = m.group("col").upper()
        col = COLOR_MAP.get(col, None)
        if m.group("coltype").upper() == "FG":
            col = FG(col, mode=mode)
        else:
            col = BG(col, mode=mode)
        return col.wrap(m.group("text"))

    def col2_repl(m: Match) -> str:
        col = m.group("color").upper()
        col = COLOR_MAP.get(col, None)
        col = FG(col, mode=mode)
        return col.wrap(m.group("text"))

    EM_RE: Pattern = re.compile(r"\*(?P<text>[^*]*)\*(?!\*)")
    I_RE: Pattern = re.compile(r"<i>(?P<text>[^<]*)</i>", re.IGNORECASE)

    def em_repl(m: Match) -> str:
        return Color(italic=True, mode=mode).wrap(m.group("text"))

    STRONG_RE: Pattern = re.compile(r"\*\*(?P<text>[^*]*)\*\*")
    B_RE: Pattern = re.compile(r"<b>(?P<text>[^<]*)</b>", re.IGNORECASE)

    def strong_repl(m: Match) -> str:
        return Color(bold=True, mode=mode).wrap(m.group("text"))

    # UNDERLINE_RE = re.compile("\*\*(?P<text>[^*]*)\*\*")
    U_RE: Pattern = re.compile(r"<u>(?P<text>[^<]*)</u>", re.IGNORECASE)

    def underline_repl(m: Pattern) -> str:
        return Color(underline=True, mode=mode).wrap(m.group("text"))

    CODE_RE: Pattern = re.compile(r"`(?P<text>[^`]*)`")
    C_RE: Pattern = re.compile(r"<code>(?P<text>[^<]*)</code>", re.IGNORECASE)

    def code_repl(m: Match) -> str:
        return Color(underline=True, mode=mode).wrap(m.group("text"))

    subs = [
        (COL_RE, col_repl),
        (COL2_RE, col2_repl),
        (EM_RE, em_repl),
        (I_RE, em_repl),
        (STRONG_RE, strong_repl),
        (B_RE, strong_repl),
        # (UNDERLINE_RE, underline_repl),
        (U_RE, underline_repl),
        (CODE_RE, code_repl),
        (C_RE, code_repl),
    ]
    ret = txt
    for pat, repl in subs:
        ret = pat.sub(repl, ret)
    return ret


class Interpolator:
    """
        A helper class for interpolating colors from a start color
        to an end color.
    """

    def __init__(self, max_val: Number, start_col: RGB, end_col: RGB):
        start_r, start_g, start_b = start_col
        end_r, end_g, end_b = end_col
        self._max_val = max_val
        self._delta = (end_r - start_r), (end_g - start_g), (end_b - start_b)
        self._start_color = start_col

    def col(self, val: Number):
        """
            Returns the color corresponding to the value val in the
            interval.
        """
        start_r, start_g, start_b = self._start_color
        delta_r, delta_g, delta_b = self._delta
        coeff = val / self._max_val
        ret_r = int(start_r + coeff * delta_r)
        ret_g = int(start_g + coeff * delta_g)
        ret_b = int(start_b + coeff * delta_b)
        return (ret_r, ret_g, ret_b)


class AbstractStatusLine:
    def start_action(self, *heading):
        """
        Starts a new statusline, printing out heading followed by a semicolon and empty status.
        """

    def end_action(
        self,
        ok: bool = True,
        message: Optional[str] = None,
        preserve_status: bool = False,
    ):
        """
        If no message is provided, the status is updated with a green ``OK`` if (`ok` is ``True``) or a
        red ``ERROR`` (if `ok` is ``False``). If a message is provided, the status is updated with this message
        instead. If preserve_status is True, the status is not updated (i.e. the last status update
        remains). Finally a new line is started.

        """

    def update(self, *args, finish: bool = False, clip: bool = True):
        """
            Updates the current status (i.e. deletes the old status and replaces it with the new status).
            Status is composed by joining the stringified arguments (similar to how print works)
        """

    def print(self, *args, sep: str = " "):
        """
        """


class StatusLine(AbstractStatusLine):
    """
        A class for creating & updating status lines (something like a progressbar, but with textual updates).
        A new status line is created by calling `start_action`, the status is updated by calling `update`
        (which has a signature similar to the print function) and the status is finished by calling `end_action`.
        A progressbar-like usage might be implemented as follows::

            from term import status
            status.start_action("Doing bar")
            for i in range(100):
                status.update(i,"% finished")
            status.end_action()

    """

    def __init__(self, OUT: IO = sys.stderr):
        self._lead_text = ""
        self._last_len = 0
        self._width = get_terminal_size(default=(0, 0))[0]
        self._out = OUT
        self._tty = os.isatty(self._out.fileno())

    def start_action(self, *heading):
        """
        Starts a new statusline, printing out heading followed by a colon and empty status.
        """
        self._clear()
        self._lead_text = " ".join([str(h) for h in heading]) + ":"
        self._last_len = 0
        self.update()

    def end_action(
        self,
        ok: bool = True,
        message: Optional[str] = None,
        preserve_status: bool = False,
    ):
        """
        If no message is provided, the status is updated with a green ``OK`` if (`ok` is ``True``) or a
        red ``ERROR`` (if `ok` is ``False``). If a message is provided, the status is updated with this message
        instead. If preserve_status is True, the status is not updated (i.e. the last status update
        remains). Finally a new line is started.

        """
        if not preserve_status:
            mlen = 0
            if message:
                message = "(" + message + ") "
                mlen += len(message)
            else:
                message = ""
            if ok:
                mlen += len("OK") + len(COLOR_OK.reset) + 5
                result_text = message + (COLOR_OK | "OK")
            else:
                mlen += len("ERROR") + len(COLOR_ERROR.reset) + 5
                result_text = message + (COLOR_ERROR | "ERROR")
            mlen += len(self._lead_text + " ")
            indent = (self._width - 10) - mlen
            if indent > 0:
                result_text = str(" " * indent) + result_text
            self.update(result_text, clip=False)
        self._last_len = 0
        print(file=self._out)
        self._out.flush()

    def update(self, *args, finish: bool = False, clip: bool = True):
        """
            Updates the current status (i.e. deletes the old status and replaces it with the new status).
            Status is composed by joining the stringified arguments (similar to how print works)
        """
        self._clear()
        if finish:
            end = "\n"
        else:
            end = ""
        out_str = " ".join([str(x) for x in args]).strip("\n")
        self._last_len = len(Color.remove_codes(self._lead_text + " " + out_str))
        if clip and self._last_len > self._width:
            out_str = out_str[: -(self._last_len - (self._width - 3))] + "..."
            self._last_len = self._width
        print(self._lead_text, out_str, end=end, flush=True, file=self._out)

    def print(self, *args, sep: str = " "):
        print(*args, flush=True, file=self._out, sep=sep)

    def progress_bar(self, title: str, width: int = 20):
        return ProgressBar(title, width, self)

    def _clear(self):
        if self._tty:
            print("\r", self._last_len * " ", "\r", end="", sep="", file=self._out)
        elif self._last_len > 0:
            print(file=self._out)


def get_char() -> str:
    """
        Reads a single character from stdin without echoing it.

        Implementation taken from https://stackoverflow.com/questions/510357/python-read-a-single-character-from-the-user
    """
    if platform.system() == "Windows":  # pragma: no cover
        import msvcrt

        return msvcrt.getch()

    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch


TermSize = Tuple[int, int]


def get_terminal_size(default: TermSize = (80, 25)) -> TermSize:
    """
    Get width and height of console; works on linux, os x, windows and cygwin
    Adapted from https://gist.github.com/jtriley/1108174
    Originally from: http://stackoverflow.com/questions/566746/how-to-get-console-window-width-in-python
    """
    current_os = platform.system()
    if current_os == "Windows":  # pragma: no cover
        size = _get_terminal_size_windows()
        if not size:
            # needed for window's python in cygwin's xterm!
            size = _get_terminal_size_tput()
    else:
        size = _get_terminal_size_linux()

    if (
        size is None
    ):  # we'll assume the standard 80x25 if for any reason we don't know the terminal size
        size = default
    return size


def _get_terminal_size_windows() -> Optional[TermSize]:  # pragma: no cover
    try:
        from ctypes import windll, create_string_buffer  # type: ignore

        stderr_handle = -12
        handle = windll.kernel32.GetStdHandle(stderr_handle)
        csbi_struct = Struct("hhhhHhhhhhh")
        csbi = create_string_buffer(csbi_struct.size)
        res = windll.kernel32.GetConsoleScreenBufferInfo(handle, csbi)
        if res:
            _, _, _, _, _, left, top, right, bottom, _, _ = csbi_struct.unpack(csbi.raw)
            return right - left + 1, bottom - top + 1
        return None
    except Exception:
        return None


def _get_terminal_size_tput() -> Optional[TermSize]:  # pragma: no cover
    # get terminal width
    # src: http://stackoverflow.com/questions/263890/how-do-i-find-the-width-height-of-a-terminal-window
    try:
        tput = local["tput"]
        cols = int(tput("cols"))
        rows = int(tput("lines"))
        return (cols, rows)
    except Exception:
        return None


def _ioctl_GWINSZ(fd) -> Optional[TermSize]:
    size = Struct("hh")
    try:
        import fcntl

        return size.unpack(fcntl.ioctl(fd, termios.TIOCGWINSZ, "1234"))
    except Exception:
        return None


def _get_terminal_size_linux() -> Optional[TermSize]:
    size = _ioctl_GWINSZ(0) or _ioctl_GWINSZ(1) or _ioctl_GWINSZ(2)
    if not size:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            size = _ioctl_GWINSZ(fd)
            os.close(fd)
        except Exception:
            pass
    if not size:
        try:
            size = (int(os.environ["LINES"]), int(os.environ["COLUMNS"]))
        except Exception:
            return None
    return size[1], size[0]


def _catchall(meth):
    def decorated(*args, **kwargs):
        try:
            return meth(*args, **kwargs)
        except Exception as ex:
            print("Got exception running", meth, args, kwargs, "EXCEPTION:", ex)

    return decorated


class MultiModeStatusLine:
    @_catchall
    def start_action(self, *args, **kwargs):
        self._impl.start_action(*args, **kwargs)

    @_catchall
    def end_action(self, *args, **kwargs):
        self._impl.end_action(*args, **kwargs)

    @_catchall
    def update(self, *args, **kwargs):
        self._impl.update(*args, **kwargs)

    @_catchall
    def print(self, *args, **kwargs):
        self._impl.print(*args, **kwargs)

    @_catchall
    def progress_bar(self, *args, **kwargs):
        return self._impl.progress_bar(*args, **kwargs)

    def set_impl(self, impl):
        self._impl = impl


_term_status_line = StatusLine()

STATUS = MultiModeStatusLine()
STATUS.set_impl(_term_status_line)


ProgressCallback = Callable[[float, str], None]


if HAVE_LITERAL:
    Thickness = Literal["|", "||", "-", "=", "", " "]
    InternalThickness = Literal["IN", "CK", "TY"]
    Alignment = Literal["c", "l", "r"]
else:
    Thickness = str
    InternalThickness = str
    Alignment = str

CellFormat = Union[Alignment, Tuple[Alignment, Color]]
FullCellFormat = Tuple[Alignment, Color]
SeparatorFormat = Union[Thickness, Tuple[Thickness, Color]]


class Table:
    """
        The table class provides for printing nice tables on the console.
        Currently it allows for two types of lines (thick ┃ / thin │).

        The following example illustrates usage

            ExampleTable = Table(('||', 'c', '|', 'c', '|', 'c')*3+('||',))
            for row in range(9):
                if row % 3 == 0:
                    ExampleTable.add_horizontal_line('=')
                else:
                    ExampleTable.add_horizontal_line('-')
                ExampleTable.add_row(list(range(row*9, (row+1)*9)))
            ExampleTable.add_horizontal_line('=')
            print(ExampleTable)

        and would print out something like:
            ┏━━┯━━┯━━┳━━┯━━┯━━┳━━┯━━┯━━┓
            ┃0 │1 │2 ┃3 │4 │5 ┃6 │7 │8 ┃
            ┠──┼──┼──╂──┼──┼──╂──┼──┼──┨
            ┃9 │10│11┃12│13│14┃15│16│17┃
            ┠──┼──┼──╂──┼──┼──╂──┼──┼──┨
            ┃18│19│20┃21│22│23┃24│25│26┃
            ┣━━┿━━┿━━╋━━┿━━┿━━╋━━┿━━┿━━┫
            ┃27│28│29┃30│31│32┃33│34│35┃
            ┠──┼──┼──╂──┼──┼──╂──┼──┼──┨
            ┃36│37│38┃39│40│41┃42│43│44┃
            ┠──┼──┼──╂──┼──┼──╂──┼──┼──┨
            ┃45│46│47┃48│49│50┃51│52│53┃
            ┣━━┿━━┿━━╋━━┿━━┿━━╋━━┿━━┿━━┫
            ┃54│55│56┃57│58│59┃60│61│62┃
            ┠──┼──┼──╂──┼──┼──╂──┼──┼──┨
            ┃63│64│65┃66│67│68┃69│70│71┃
            ┠──┼──┼──╂──┼──┼──╂──┼──┼──┨
            ┃72│73│74┃75│76│77┃78│79│80┃
            ┗━━┷━━┷━━┻━━┷━━┷━━┻━━┷━━┷━━┛
    """

    TY_TY_CK_CK = "┏"
    CK_TY_TY_CK = "┓"
    TY_CK_CK_TY = "┗"
    CK_CK_TY_TY = "┛"
    CK_TY_CK_TY = "━"
    TY_CK_TY_CK = "┃"
    CK_CK_CK_CK = "╋"
    CK_TY_CK_CK = "┳"
    CK_CK_CK_TY = "┻"
    CK_CK_TY_CK = "┫"
    TY_CK_CK_CK = "┣"

    CK_TY_CK_IN = "┯"
    CK_IN_CK_TY = "┷"
    CK_IN_CK_IN = "┿"
    TY_CK_IN_CK = "┠"
    IN_CK_TY_CK = "┨"

    IN_TY_IN_CK = "┰"
    IN_CK_IN_TY = "┸"
    IN_CK_IN_CK = "╂"
    TY_IN_CK_IN = "┝"
    CK_IN_TY_IN = "┥"

    TY_TY_IN_IN = "┌"
    IN_TY_TY_IN = "┐"
    TY_IN_IN_TY = "└"
    IN_IN_TY_TY = "┘"
    IN_TY_IN_TY = "─"
    TY_IN_TY_IN = "│"
    IN_IN_IN_IN = "┼"
    IN_TY_IN_IN = "┬"
    IN_IN_IN_TY = "┴"
    IN_IN_TY_IN = "┤"
    TY_IN_IN_IN = "├"

    TY_TY_TY_TY = " "

    @classmethod
    def _get_char(cls, left, up, right, down):
        return getattr(Table, left + "_" + up + "_" + right + "_" + down)

    ASCII_TO_KEYWORD: Dict["Thickness", "InternalThickness"] = {
        "|": "IN",  # thin lines
        "-": "IN",  # thin lines
        "||": "CK",  # thick lines
        "=": "CK",  # thick lines
        "": "TY",  # no lines
        " ": "TY",  # no lines
    }

    class HorizontalLine:
        """
            Represents a horizontal line.

            (For internal use only)
        """

        def __init__(
            self, table: "Table", thickness: InternalThickness, style: Color = None
        ):
            """
                @p table        the table to which this line belongs (used for calculating widths, crossings, etc.)
                @p thickness    should be one of 'IN', 'CK', 'TY'
                @p style        optional style wrapper for the line (e.g. color)
            """
            self.table: "Table" = table
            self.thickness: InternalThickness = thickness
            self.style: Color = style

        def render(self, position: str) -> str:
            """
                Returns a string with the rendered line

                @p position should be one of 'top', 'middle', 'bottom', 'isolated'

                Where the position determines what characters will be used for crossings.
            """
            if position == "isolated":
                char = self.table._get_char(self.thickness, "TY", self.thickness, "TY")
                ln = self.table.num_cols + sum(self.table._col_widths)
                ln = [char * ln]

            if position == "top":
                start_char = self.table._get_char(
                    "TY", "TY", self.thickness, self.table._sep_weights[0]
                )
                stop_char = self.table._get_char(
                    self.thickness, "TY", "TY", self.table._sep_weights[-1]
                )
                hor_char = self.table._get_char(
                    self.thickness, "TY", self.thickness, "TY"
                )
                ln = [start_char]
                for idx, col_w in enumerate(self.table._col_widths):
                    ln.append(col_w * hor_char)
                    if idx < len(self.table._col_widths) - 1:
                        ln.append(
                            self.table._get_char(
                                self.thickness,
                                "TY",
                                self.thickness,
                                self.table._sep_weights[idx + 1],
                            )
                        )
                ln.append(stop_char)

            if position == "middle":
                start_char = self.table._get_char(
                    "TY",
                    self.table._sep_weights[0],
                    self.thickness,
                    self.table._sep_weights[0],
                )
                stop_char = self.table._get_char(
                    self.thickness,
                    self.table._sep_weights[-1],
                    "TY",
                    self.table._sep_weights[-1],
                )
                hor_char = self.table._get_char(
                    self.thickness, "TY", self.thickness, "TY"
                )
                ln = [start_char]
                for idx, col_w in enumerate(self.table._col_widths):
                    ln.append(col_w * hor_char)
                    if idx < len(self.table._col_widths) - 1:
                        ln.append(
                            self.table._get_char(
                                self.thickness,
                                self.table._sep_weights[idx + 1],
                                self.thickness,
                                self.table._sep_weights[idx + 1],
                            )
                        )
                ln.append(stop_char)

            if position == "bottom":
                start_char = self.table._get_char(
                    "TY", self.table._sep_weights[0], self.thickness, "TY"
                )
                stop_char = self.table._get_char(
                    self.thickness, self.table._sep_weights[-1], "TY", "TY"
                )
                hor_char = self.table._get_char(
                    self.thickness, "TY", self.thickness, "TY"
                )
                ln = [start_char]
                for idx, col_w in enumerate(self.table._col_widths):
                    ln.append(col_w * hor_char)
                    if idx < len(self.table._col_widths) - 1:
                        ln.append(
                            self.table._get_char(
                                self.thickness,
                                self.table._sep_weights[idx + 1],
                                self.thickness,
                                "TY",
                            )
                        )
                ln.append(stop_char)

            ln = "".join(ln)
            if self.style:
                return self.style | ln
            return ln

    def __init__(self, row_format: List[Union[CellFormat, SeparatorFormat]]):
        """
            Creates a new table with the given row format. A row format
            is a sequence alternating between specifying separator formats
            and cell formats. It needs to start and end with a separator format.

            A cell format specifies cell's alignment ('c' - centered, 'l' - left, 'r' - right)
            and, optionally the style (fg, bg color, bold, italic, ...)

            A separator format specifies the thickness of separating lines
            ('||' - thick, '|' - thin, '' - no separator) and, optionally
            the style (fg, bg color, bold, italic, ...). Note that it is
            not recommended to use a style for the separators, due to terminal
            limitations.
        """
        self._rows: List[Tuple[str, FullCellFormat]] = []
        self._col_widths: List[int] = []
        self._row_heights: List[int] = []
        self._sep_weights: List[InternalThickness] = []
        self._sep_styles: List[Color] = []
        self._col_styles: List[FullCellFormat] = []

        for elt in row_format:
            if isinstance(elt, tuple):
                spec, style = elt
            else:
                spec, style = elt, NO_STYLE
            if spec == "||":
                self._sep_weights.append("CK")
                self._sep_styles.append(style)
            elif spec == "|":
                self._sep_weights.append("IN")
                self._sep_styles.append(style)
            elif elt == "":
                self._sep_weights.append("TY")
                self._sep_styles.append(style)
            else:
                self._col_styles.append((spec, style))
                self._col_widths.append(0)

        if len(self._sep_styles) != len(self._col_styles) + 1:
            raise Exception("Invalid row format (missing last separator spec?)")

        self._separators = [
            self._sep_styles[idx]
            | self._get_char("TY", self._sep_weights[idx], "TY", self._sep_weights[idx])
            for idx in range(len(self._sep_styles))
        ]

    @property
    def num_cols(self) -> int:
        """
            The number of columns the table has
        """
        return len(self._col_widths)

    @property
    def num_rows(self) -> int:
        """
            The number of rows the table has.
        """
        return len(self._rows)

    def _format_cell(
        self,
        text: str,
        style: Tuple[Alignment, Color],
        width: Optional[int] = None,
        height: Optional[int] = None,
    ) -> str:
        """
            Formats @p text to fit into @p width, padding with spaces,
            if necessary and applying a style to the result.

            @p text     the text to format
            @p style    (Alignment, Color) specifies the alignment and style to apply
            @p width    the width to fit into
            @p height   unused

            @Note: if width is too small, the result will not fit into it.
        """
        spec, wrapper = style

        if not width:
            return wrapper | text

        clean_txt = Color.remove_codes(text)

        if len(clean_txt) > width:
            return wrapper | text

        prefix = ""
        postfix = ""
        free_space_size = width - len(clean_txt)
        if spec == "c":
            prefix = " " * (free_space_size // 2)
            postfix = " " * (free_space_size // 2 + free_space_size % 2)
        elif spec == "l":
            postfix = " " * (free_space_size)
        elif spec == "r":
            prefix = " " * (free_space_size)

        return wrapper | (prefix + text + postfix)

    def add_row(
        self,
        cols: Iterable[Union[Tuple[Any, Color], Tuple[Any, Color, Alignment], Any]],
    ):
        """
            Adds a row to the table.

            @p cols is an iterable of cells. Each cell can be either:

                a pair (content, color) or
                a triple (content, color, alignment) or
                just content

            Content is converted to string using str. If color is not specified,
            it takes the appropriate default column color from the table definition.
            If alignment is not specified, it takes the default column alignment.

            @Note: The number of cells needs to match the number defined when the
            table was constructed.
        """
        row = []
        for idx, col in enumerate(cols):
            if isinstance(col, tuple):
                if len(col) == 2:
                    col, style = col
                    style: FullCellFormat = (self._col_styles[idx][0], style)
                else:
                    col, color, alignment = col
                    style: FullCellFormat = (alignment, color)
            else:
                style = self._col_styles[idx]
            if not isinstance(col, str):
                col = str(col)
            col_txt_len = len(Color.remove_codes(col))
            self._col_widths[idx] = max(col_txt_len, self._col_widths[idx])
            row.append((col, style))
        self._rows.append(row)

    def add_horizontal_line(self, thickness: Thickness = "-", style: Color = None):
        """
            Adds a horizontal line to the table.
        """
        self._rows.append(
            self.HorizontalLine(self, self.ASCII_TO_KEYWORD[thickness], style)
        )

    def __str__(self):
        """
            Returns an ascii rendering of the table.
        """
        rendered_rows = []
        for ridx, row in enumerate(self._rows):
            if isinstance(row, Table.HorizontalLine):
                if ridx == 0:
                    rendered_rows.append(row.render("top"))
                elif ridx == self.num_rows - 1:
                    rendered_rows.append(row.render("bottom"))
                else:
                    rendered_rows.append(row.render("middle"))
            else:
                rendered_row = [self._separators[0]]
                for cidx, cell in enumerate(row):
                    if isinstance(cell, tuple):
                        cell, style = cell
                    else:
                        style = self._col_styles[cidx]
                    rendered_row.append(
                        self._format_cell(cell, style, self._col_widths[cidx])
                    )
                    rendered_row.append(self._separators[cidx + 1])
                rendered_rows.append("".join(rendered_row))
        return "\n".join(rendered_rows)


class ProgressBar:
    """
        Provides  simple progressbar which can be used, e.g. as follows::

            from term import ProgressBar

            progress = ProgressBar("Iterating:")
            for i in range(250):
                cnt = do_some_stuff(i)
                progress.update(i/250, " ("+str(cnt)+" processed)")
            progress.done()

    """

    def __init__(
        self,
        title: str,
        width: int = 20,
        status_line=STATUS,
        bar_char: str = " ",
        start_color: RGB = WHITE,
        end_color: RGB = WHITE,
    ):
        self._title = title
        self._width = width
        self._status = status_line
        self._status.start_action(title)
        self._interpolator = Interpolator(self._width, start_color, end_color)
        self._bar_char = bar_char

    def update(self, fraction: float, message: str):
        """
            Updates the progressbar to a fraction of its total width given by `fraction`
            and also prints out the message at the end of the (full) bar.
        """
        if fraction >= 1:
            bar_len = self._width
        else:
            bar_len = int(fraction * self._width)
        rem_len = self._width - bar_len
        bar = "".join(
            [(BG(self._interpolator.col(n)) | self._bar_char) for n in range(bar_len)]
        )
        # self._status.update(bar_len*"#"+rem_len*" ", message)
        self._status.update(bar + (rem_len * " "), message)

    def done(self, message: str = ""):
        """
            Finishes the job and proceeds to the next line.
        """
        if message:
            success = False
        else:
            message = "OK"
            success = True
        self._status.end_action(ok=success, message=message, preserve_status=success)
