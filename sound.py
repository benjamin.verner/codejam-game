import os
import random
import sys
import threading
import time

from pathlib import Path
from typing import List, Dict, Optional, Tuple

import fluidsynth

NOTES_MAP = {
    "C": 0,
    "C#": 1,
    "Db": 1,
    "D": 2,
    "D#": 3,
    "Eb": 3,
    "E": 4,
    "F": 5,
    "F#": 6,
    "Gb": 6,
    "G": 7,
    "G#": 8,
    "Ab": 8,
    "A": 9,
    "A#": 10,
    "Hb": 10,
    "H": 11,
}
CHANNEL = 0


def init_sf():
    fs = fluidsynth.Synth()
    if sys.platform == 'win32':
        fs.start()
    else:
        fs.start(driver="pulseaudio")

    try:
        wd = Path(sys._MEIPASS)
    except AttributeError:
        wd = Path(os.getcwd())
    sfid = fs.sfload(str((wd/"Yamaha-Grand-Lite-SF-v1.1.sf2").absolute()))
    # sfid = fs.sfload("TimGM6mb.sf2")
    fs.program_select(0, sfid, 0, 0)
    fs.set_reverb(roomsize=0.0, damping=0.1, width=50, level=1)
    return fs


Pitch = int
Chord = List[Pitch]
Duration = float


def apply_vibrato(fs: fluidsynth.Synth, duration: Duration):
    for step in range(50):
        fs.pitch_bend(CHANNEL, random.randint(0, 2000))
        time.sleep(duration/50)


def random_sound():
    fs = init_sf()
    bg = RandomBackgroundSound(fs)
    sleep_duration = 0.01
    while True:
        bg.randomize()
        time.sleep(sleep_duration)
        sleep_duration = 0.005*random.randint(2, 10)


class Rand:
    def __init__(self):
        self._process = multiprocessing.Process(target=random_sound, args=())

    def start(self):
        self._process.start()

    def stop(self):
        self._process.kill()


class BGSound:
    def __init__(self, fs):
        self._fs = fs
        self._ev = threading.Event()
        self._bgs = RandomBackgroundSound(self._fs)

    def start(self):
        def rands():
            while True:
                self._bgs.randomize()
                if self._ev.wait(0.1):
                    self._bgs._off()
                    return
        self._t = threading.Thread(target=rands)
        self._t.start()

    def stop(self):
        self._ev.set()
        self._t.join()
        self._bgs._off()


class RandomBackgroundSound:
    def __init__(self, fs):
        self._active_notes = []
        self.fs = fs

    def _off(self):
        for n in self._active_notes:
            self.fs.noteoff(CHANNEL, n)
        self._active_notes = []

    def _on(self):
        for n in self._active_notes:
            self.fs.noteon(CHANNEL, Score.BASE_NOTE+n, random.randint(50, 100))

    def randomize(self):
        choice = random.randint(0, 15)
        if choice == 0:
            return
        elif choice == 1:
            self._off()
            return
        elif choice in range(2, 10):
            self.fs.pitch_bend(CHANNEL, random.randint(0, 10000))
            return
        self._off()
        self._active_notes = random.choices(range(127), k=random.randint(1, 6))
        self._on()


class Score:
    WIN_ALTERNATIVE_MAP = {
        '🌓': '¶',
        '🌗': '¨',
        '🔴': '®',
        '🔵': '@',
        '🔶': '%',
        '🔷': '&',
        '🏵': '©',
        '💠': '#',
        '⭓': '©',
        '🞜': '§',
        '～': '¥',
        'ꯠ': '¤',
        '⋈': '»',
        'ꚩ': '¿',
        '⩤': 'Þ',
        '⩥': '£',
        '⦔': '*'
    }
    WIN_ALTERNATIVE_MAP_INVERSE = {v: k for k, v in WIN_ALTERNATIVE_MAP.items()}

    @classmethod
    def translate_to_win(cls, src):
        return ''.join(map(lambda x: cls.WIN_ALTERNATIVE_MAP[x], src))

    @classmethod
    def translate_from_win(cls, src):
        return ''.join(map(lambda x: cls.WIN_ALTERNATIVE_MAP_INVERSE[x], src))

    BASE_NOTE = 60
    SYMBOL_2_PITCH: Dict[str, Pitch] = {
        '🔴': 0,
        '🔵': 2,
        '🔶': 4,
        '🔷': 6,
        '🏵': 8,
        '💠': 10,
        '⭓': 12,
    }
    OPS = {
        '🞜': 'silence',
        '～': 'hold',
    }
    MODS = {
        'ꯠ': 'vibrate',
        '⋈': 'faster',
        'ꚩ': 'slower',
        '⩤': 'up',
        '⩥': 'down',
        '⦔': 'reset'
    }
    BEGIN = '🌓'
    END = '🌗'

    CHANNEL = 0

    def __init__(self, fs: fluidsynth.Synth):
        self._fs = fs
        self._reset()

    def _reset(self):
        self._speed = 1
        self._duration = 0.5
        self._pitch_mod = 0
        self._vibrate = False

    def _off(self):
        for i in range(127):
            self._fs.noteoff(self.CHANNEL, i)

    def _wait(self, duration: Duration):
        time.sleep(duration)

    def _play_chord(self, chord: Chord, duration: Duration, vibrate: bool = False):
        for note in chord:
            self._fs.noteon(self.CHANNEL, self.BASE_NOTE + note, 100+random.randint(0, 10))
        if vibrate:
            apply_vibrato(self._fs, duration)
        else:
            time.sleep(duration)
        for note in chord:
            self._fs.noteoff(0, self.BASE_NOTE + note)

    def normalize(self, src: str) -> List[Tuple[Optional[Chord], Duration, bool]]:
        self._reset()
        ret = []
        begin = False
        last_event = (
            None,
            0,
            False
        )
        for elt in src:
            try:
                if begin:
                    if elt in self.SYMBOL_2_PITCH:
                        pitch = self.SYMBOL_2_PITCH[elt]
                        last_event = (
                            list(sorted([pitch+self._pitch_mod])),
                            self._duration/self._speed,
                            self._vibrate
                        )
                        ret.append(last_event)
                    elif elt in self.OPS:
                        op = self.OPS[elt]
                        if self.OPS[elt] == 'hold':
                            ret.append(last_event)
                        elif self.OPS[elt] == 'silence':
                            if last_event[0] is None:
                                ret[-1] = (
                                    None,
                                    last_event[1]+self._duration/self._speed,
                                    False
                                )
                                last_event = (
                                    None,
                                    self._duration/self._speed,
                                    False
                                )
                            else:
                                last_event = (
                                    None,
                                    self._duration/self._speed,
                                    False
                                )
                                ret.append(last_event)
                    elif elt in self.MODS:
                        mod = self.MODS[elt]
                        if mod == 'up':
                            self._pitch_mod += 1
                        elif mod == 'down':
                            self._pitch_mod -= 1
                        elif mod == 'faster':
                            self._speed *= 2
                        elif mod == 'slower':
                            self._speed /= 2
                        elif mod == 'vibrate':
                            self._vibrate = True
                        elif mod == 'reset':
                            self._reset()
                    elif elt == self.END:
                        begin = False
                elif elt == self.BEGIN:
                    begin = True
            except Exception as e:
                return []
        if ret and ret[0][0] is None:
            ret = ret[1:]
        if ret and ret[-1][0] is None:
            ret = ret[:-1]
        self._reset()
        return ret

    def play_normalized(self, seq: List[Tuple[Optional[Chord], Duration, bool]]):
        for chord, speed, vibrato in seq:
            if chord:
                self._play_chord(chord, speed, vibrato)
            else:
                self._wait(speed)
        self._reset()

    def equivalent(self, srcA: str, srcB: str) -> bool:
        seqA = self.normalize(srcA)
        seqB = self.normalize(srcB)

        if len(seqA) != len(seqB):
            return False

        for a, b in zip(seqA, seqB):
            if (a[0] is None or b[0] is None):
                if a[1] != b[1]:
                    return False
                continue

            if len(a[0]) != len(b[0]):
                return False

            for pA, pB in zip(a[0], b[0]):
                if pA != pB:
                    return False

            if  a[1] != b[1] or a[2] != b[2]:
                return False

        return True

    def play(self, src: str):
        self._reset()
        seq = self.normalize(src)
        self.play_normalized(seq)
        self._reset()


if __name__ == "__main__":
    sf = init_sf()
    score = Score(sf)
    print(score.equivalent("ꯠꯠ🔴🌓🔴🔴", "🌓⩥⩥🔵"))
    print(score.equivalent("ꯠꯠ🔴🌓🔴🔴⋈🞜🞜ꚩ🔴", "🌓⩥⩥🔵～🞜🔵🞜🞜🌗🔵🔵🔵🔵"))
    print(score.normalize("ꯠꯠ🔴🌓🔴🔴⋈🞜🞜ꚩ🔴"))
    print(score.normalize("🌓⩥⩥🔵～🞜🔵🞜🞜🌗🔵🔵🔵🔵"))
    score.play("🌓ꯠ🔴～🔶⩤🔷⩥🞜🔴🔶⩤🔷⩥⦔🞜⋈🔶🔶🔵🔶ꚩ⩤🔶⩥🔵")




