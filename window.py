from collections import defaultdict

from mytypes import Pos, X, Y, ADD, DELTA_X, DELTA_Y
from view import View

try:
    import curses
    HAVE_CURSES = True
except:
    HAVE_CURSES = False

try:
    import asciimatics
    from asciimatics.screen import Screen as ASCIIMATICSScreen
    HAVE_ASCIIMATICS = True
except:
    HAVE_ASCIIMATICS = False


class Surface:
    """
        An abstract surface of a given height and width
        which can be drawn upon.
    """
    def __init__(self, height: int, width: int):
        self._h = height
        self._w = width

    # Curses like interface
    def addch(self, y, x, char, attr=None): self.addstr(y, x, char, attr)
    def addstr(self, y, x, text, attr=None): pass
    def getmaxyx(self) -> Pos: return (self._h, self._w)
    def refresh(self): pass
    def clear(self): pass

    @property
    def size(self) -> Pos: return self.getmaxyx()

    @property
    def width(self): return self._w

    @property
    def height(self): return self._h


class Window(Surface):
    """
        Represents a physical rectangle on a screen.

        Must fit within the screen.
    """
    TYPE_CURSES = 0
    TYPE_ASCIIMATICS = 1

    def __init__(self, tp, impl, y, x: Pos, height: int, width: int):
        super().__init__(height, width)
        self._impl = impl
        self._pos = (y, x)
        self._type = tp
        self._wnd = None
        if self._type == self.TYPE_CURSES and HAVE_CURSES:
            max_y, max_x = self._impl.getmaxyx()
            if y+height > max_y or x+width > max_x:
                raise Exception(f"Window ({y},{x}) -- ({y+height}, {x+width}) too large to fit in ({max_y}, {max_x})")
            self._wnd = curses.newwin(height, width, y, x)
        elif self._type == self.TYPE_ASCIIMATICS and HAVE_ASCIIMATICS:
            max_y, max_x = self._impl.dimension
            if y+height > max_y or x+width > max_x:
                raise Exception(f"Window ({y},{x}) -- ({y+height}, {x+width}) too large to fit in ({max_y}, {max_x})")
        else:
            raise Exception(f"Unsupported type {tp}")

    def map_to_screen(self, window_pos: Pos):
        return window_pos

    def addch(self, y, x, char, attr=None):
        if self._type == self.TYPE_CURSES:
            return self._wnd.addch(y, x, char)
        elif self._type == self.TYPE_ASCIIMATICS:
            screen_pos = self.map_to_screen((y, x))
            self._impl.print_at(char, Y(screen_pos), X(screen_pos))

    def addstr(self, y, x, text, attr=None):
        if self._type == self.TYPE_CURSES:
            return self._wnd.addstr(y, x, text)
        elif self._type == self.TYPE_ASCIIMATICS:
            screen_pos = self.map_to_screen((y, x))
            self._impl.print_at(text, Y(screen_pos), X(screen_pos))

    def refresh(self):
        if self._type == self.TYPE_CURSES:
            self._wnd.refresh()
            self._impl.refresh()
        elif self._type == self.TYPE_ASCIIMATICS:
            self._impl.refresh()

    def clear(self):
        if self._type == self.TYPE_CURSES:
            self._wnd.clear()
        elif self._type == self.TYPE_ASCIIMATICS:
            self._impl.clear()


    def derwin(self, height: int, width: int, win_y: Pos, win_x: Pos):
        return Window(self._type, self._impl, win_y, win_x, height, width)


class VirtualWindow(Surface):
    """
        Represents a virtual window (of arbitrary size)
    """
    def __init__(self, height: int, width: int, backing_screen: Window):
        self._h = height
        self._w = width
        self._buffer = [[]]
        self._scr = backing_screen

    def addstr(self, y, x, text, attr=None):
        if y >= len(self._buffer):
            self._buffer = [[] for _ in range(y+1)]
        line = self._buffer[y]
        if x+len(text) >= len(line):
            line.extend(list(' '*(x+len(text)-len(line))))
        for idx, ch in enumerate(text):
            line[x+idx] = ch

    @property
    def buf(self): return self._buffer

    def get_sized_content_clip(self, ul: Pos, height: int, width: int):
        return self.get_content_clip(ul, (Y(ul)+height, X(ul)+width))

    def get_content_clip(self, ul: Pos, lr: Pos):
        ret = []
        for ln in self.buf[Y(ul):Y(lr)]:
            ret.append(''.join(ln[X(ul): X(lr)]))
        return ret

    def render(self, screen: Window, screen_pos: Pos, virtual_ul: Pos, virtual_lr: Pos):
        scx, scy = screen_pos
        sc_height, sc_width = screen.size
        clip_width = DELTA_X(virtual_ul, virtual_lr)
        clip_height = DELTA_Y(virtual_ul, virtual_lr)
        clip_width = min(clip_width, sc_width-scx)
        clip_height = min(clip_height, sc_height-scy)
        #raise Exception(f"W:{clip_width}, H:{clip_height}, SP:{screen_pos} ({sc_height}, {sc_width}), UL:{virtual_ul} UR:{virtual_lr}")
        for idx, ln in enumerate(self.get_sized_content_clip(virtual_ul, clip_height, clip_width)):
            screen.addstr(scy+idx, scx, ''.join(ln))

    def refresh(self, *args):
        if args:
            vy, vx, scr_ul_y, scr_ul_x, scr_lr_y, scr_lr_x = args
            height = scr_lr_y - scr_ul_y
            width = scr_lr_x - scr_ul_x
            self.render(self._scr, (scr_ul_y, scr_ul_x), (vy, vx), (vy+height, vy+width))
            self._scr.refresh()

