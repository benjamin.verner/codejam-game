import os
import random
import time
import fluidsynth



#fs.noteon(0, 60, 30)
#fs.noteon(0, 67, 30)
#fs.noteon(0, 76, 30)

#time.sleep(1.0)

#fs.noteoff(0, 60)
#fs.noteoff(0, 67)
#fs.noteoff(0, 76)

#time.sleep(1.0)

#fs.delete()


NOTES_MAP = {
    "C": 0,
    "C#": 1,
    "Db": 1,
    "D": 2,
    "D#": 3,
    "Eb": 3,
    "E": 4,
    "F": 5,
    "F#": 6,
    "Gb": 6,
    "G": 7,
    "G#": 8,
    "Ab": 8,
    "A": 9,
    "A#": 10,
    "Hb": 10,
    "H": 11,
}

def init_sf():
    fs = fluidsynth.Synth()
    fs.start(driver="pulseaudio")
    sfid = fs.sfload("Yamaha-Grand-Lite-SF-v1.1.sf2")
    # sfid = fs.sfload("TimGM6mb.sf2")
    fs.program_select(0, sfid, 0, 0)
    fs.set_reverb(roomsize=0.0, damping=0.1, width=50, level=1)
    return fs


CHANNEL = 0


def randomize(fs, chord, duration):
    for step in range(10):
        mods = {note: NOTES_MAP[note.upper()]+60+random.randint(0, 2) for note in chord}
        for note in chord:
            fs.noteon(CHANNEL, mods[note], 100+random.randint(0, 10))
        time.sleep(duration/10)
        for note in chord:
            fs.noteoff(CHANNEL, mods[note])


def apply_vibrato(fs, duration):
    for step in range(50):
        fs.pitch_bend(CHANNEL, random.randint(0, 2000))
        time.sleep(duration/50)


def play_chord(fs, notes, duration, vibrate=False):
    for note in notes:
        note = NOTES_MAP[note.upper()]
        fs.noteon(0, note+60, 100+random.randint(0, 10))
    if vibrate:
        apply_vibrato(fs, duration)
    else:
        time.sleep(duration)
    for note in notes:
        note = NOTES_MAP[note.upper()]
        fs.noteoff(0, note+60)


def play_score(fs, score, vibrate=False):
    for chord in score:
        #vibrate(fs, chord, 2)
        play_chord(fs, chord, 1, vibrate)


SCORE = [
    ("C", "E", "G"),
    ("C", "F", "A"),
    ("D", "G", "H")
]

OPS = {
    '🌲': 'split',
    '🌊': 'vibrate',
    '🏵': 'up',
    '🔴': 'C',
    '🔵': 'E',
    '🌓': 'start',
    '🌗': 'stop',
    '💠': 'down',
    '🔶': 'fast',
    '🔷': 'slow',
    '🔰': 'swing',
    '～': 'tilda',
    'ꯠ': 'test',
    'ꚩ◍🞜⋈∰⧂⦔⭓⨳ꯠ～':''
}


class Score:
    BASE_NOTE = 60
    SYMBOL_2_PITCH = {
        '🔴': 0,
        '🔵': 2,
        '🔶': 4,
        '🔷': 6,
        '🏵': 8,
        '💠': 10,
        '⭓': 12,
    }
    OPS = {
        '🞜': 'silence',
        '～': 'hold',
    }
    MODS = {
        'ꯠ': 'vibrate',
        '⋈': 'faster',
        'ꚩ': 'slower',
        '⩤': 'up',
        '⩥': 'down',
        '⦔': 'reset'
    }
    BEGIN = '🌓'
    END = '🌗'

    CHANNEL = 0

    def __init__(self, fs):
        self._fs = fs
        self._reset()

    def _reset(self):
        self._speed = 1
        self._duration = 0.5
        self._pitch_mod = 0
        self._vibrate = False

    def _off(self):
        for i in range(127):
            self._fs.noteoff(self.CHANNEL, i)

    def _wait(self):
        time.sleep(self._duration/self._speed)

    def play_chord(self, chord, duration, vibrate=False):
        for note in chord:
            self._fs.noteon(self.CHANNEL, self.BASE_NOTE + note, 100+random.randint(0, 10))
        if vibrate:
            apply_vibrato(self._fs, duration)
        else:
            time.sleep(duration)
        for note in chord:
            self._fs.noteoff(0, self.BASE_NOTE + note)

    def play(self, src):
        self._reset()
        begin = False
        try:
            for elt in src:
                if begin:
                    if elt in self.SYMBOL_2_PITCH:
                        pitch = self.SYMBOL_2_PITCH[elt]
                        last_chord = [pitch+self._pitch_mod]
                        self.play_chord(last_chord, self._duration/self._speed, self._vibrate)
                    elif elt in self.OPS:
                        op = self.OPS[elt]
                        if self.OPS[elt] == 'hold':
                            self.play_chord(last_chord, self._duration/self._speed)
                        elif self.OPS[elt] == 'silence':
                            self._wait()
                    elif elt in self.MODS:
                        mod = self.MODS[elt]
                        if mod == 'up':
                            self._pitch_mod += 1
                        elif mod == 'down':
                            self._pitch_mod -= 1
                        elif mod == 'faster':
                            self._speed *= 2
                        elif mod == 'slower':
                            self._speed /= 2
                        elif mod == 'vibrate':
                            self._vibrate = True
                        elif mod == 'reset':
                            self._reset()
                    elif elt == self.END:
                        begin = False
                elif elt == self.BEGIN:
                    begin = True
        except Exception as e:
            print(e)
            self._off()
        self._reset()


if __name__ == "__main__":
    sf = init_sf()
    score = Score(sf)
    score.play("🌓ꯠ🔴～🔶⩤🔷⩥🞜🔴🔶⩤🔷⩥⦔🞜⋈🔶🔶🔵🔶ꚩ⩤🔶⩥🔵")
    #play_score(sf, SCORE, vibrate=True)


