from typing import List, Tuple

import curses

stdscr = curses.initscr()

Buffer = List[List[str]]

def contents(buf: Buffer) -> str: return '\n'.join([''.join(ln) for ln in buf])

Pos = Tuple[int, int]
def X(pos: Pos) -> int: return pos[1]
def Y(pos: Pos) -> int: return pos[0]
def ADD(posa: Pos, posb: Pos) -> Pos: return (posa[0]+posb[0], posa[1]+posb[1])
def DELTA_X(posa, posb): return abs(X(posa)-X(posb))
def DELTA_Y(posa, posb): return abs(Y(posa)-Y(posb))


def valid_buf_pos(buf: Buffer, pos: Pos) -> bool: return 0 <= Y(pos) < len(buf) and 0 <= X(pos) < len(buf[Y(pos)])


def log(*message):
    message = ' '.join([str(x) for x in message])
    height = 0
    screen_width = X(stdscr.getmaxyx())
    center = len(message) // 2
    position = screen_width // 2 - center
    stdscr.addstr(height, position, message)
    stdscr.refresh()

