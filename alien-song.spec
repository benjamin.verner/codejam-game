# -*- mode: python -*-
block_cipher = None

a = Analysis(
             ['main.py'],
             pathex=['/home/jonathan/zdroj/codejam-game'],
             binaries=[
                ('assets/libfluidsynth.dll', '.'),
            ],
             datas=[
                ('assets/Yamaha-Grand-Lite-SF-v1.1.sf2', '.'),
                ('map.txt', '.'),
             ],
             hiddenimports=[
                '_cffi_backend',
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='alien-song',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True,
          #icon='doc/_static/pyvallex-icon.ico'
)
